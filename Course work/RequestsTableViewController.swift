//
//  RequestsTableViewController.swift
//  Course work
//
//  Created by Влад  on 28.04.2020.
//  Copyright © 2020 Влад . All rights reserved.
//

import UIKit
import RealmSwift

class RequestsTableViewController: UITableViewController {
    var realm : Realm!

    struct RequestData{
        var id: Int?
        var title: String?
        init(id: Int, title: String) {
            self.id = id
            self.title = title
        }
    }
    
    var ownersList: Results<Owner> {
        get {
            return realm.objects(Owner.self)
        }
    }

    var premisesList: Results<Premise> {
        get {
            return realm.objects(Premise.self)
        }
    }

    var billsList: Results<Bill> {
        get {
            return realm.objects(Bill.self)
        }
    }
    
    
    var requests = [RequestData]()
    var selectedRequestID: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()
        
//        var bill1 = Bill()
//        bill1.id = incrementBillID()
//        bill1.electricityBill = 303
//        bill1.gasBill = 323
//        bill1.heatingBill = 99.8
//        bill1.waterBill = 89.2
//        bill1.invoiceReceiptDate = Date()
//        bill1.isClosed = false
//        bill1.premise = premisesList[6]
//        try! self.realm.write({
//            self.realm.objects(Premise.self)[6].bills.append(bill1)
//            self.realm.add(bill1)
//        })
//        try! self.realm.write {
//            let allOwners = realm.objects(Owner.self)
//            let allPremises = realm.objects(Premise.self)
//            let allBills = realm.objects(Bill.self)
//            realm.delete(allOwners)
//            realm.delete(allBills)
//            realm.delete(allPremises)
//        }
//        var owner1 = Owner()
//        owner1.id = self.incrementOwnerID()
//        owner1.fullName = "Vanya Ivanov"
//        owner1.passportId = "191919233"
//        try! self.realm.write({
//            self.realm.add(owner1)
//        })
//        var owner2 = Owner()
//        owner2.id = self.incrementOwnerID()
//        owner2.fullName = "Alexey Alexeev"
//        owner2.passportId = "834343428"
//        try! self.realm.write({
//            self.realm.add(owner2)
//        })
//        var owner3 = Owner()
//        owner3.id = self.incrementOwnerID()
//        owner3.fullName = "Alexandr Alexandrov"
//        owner3.passportId = "210414041"
//        try! self.realm.write({
//            self.realm.add(owner3)
//        })
//        var owner4 = Owner()
//        owner4.id = self.incrementOwnerID()
//        owner4.fullName = "Tim Cook"
//        owner4.passportId = "121212121"
//        try! self.realm.write({
//            self.realm.add(owner4)
//        })
//        var owner5 = Owner()
//        owner5.id = self.incrementOwnerID()
//        owner5.fullName = "John Doe"
//        owner5.passportId = "111111111"
//        try! self.realm.write({
//            self.realm.add(owner5)
//        })
        

//        var premise1 = Premise()
//        premise1.id = incrementPremiseID()
//        premise1.owner = ownersList[4]
//        premise1.address = "California, SF, 23131"
//        premise1.personalAccount = "657489300"
//        try! self.realm.write{
//            self.realm.objects(Owner.self)[4].premises.append(premise1)
//            self.realm.add(premise1)
//        }

        let req1 = RequestData(id: 1, title: "Простий запит на вибірку")
        let req2 = RequestData(id: 2, title: "Запит на вибірку з використанням between....and")
        let req3 = RequestData(id: 3, title: "Запит на вибірку з використанням in")
        let req4 = RequestData(id: 4, title: "Запит на вибірку з використанням DISTINCT")
        let req5 = RequestData(id: 5, title: "Запит з функцією min або max")
        let req6 = RequestData(id: 6, title: "Запит з функцією sum або avg")
        let req7 = RequestData(id: 7, title: "Запит з функцією count")
        let req8 = RequestData(id: 8, title: "Запит на вибірку з використанням агрегатної функції і виведенням ще декількох полів")
        let req9 = RequestData(id: 9, title: "Запит на вибірку з використанням агрегатної функції і умовою на вибірку поля.")
        let req11 = RequestData(id: 11, title: "Запит на вибірку з використанням агрегатної функції і умовою на агрегатну функцію.")
        let req12 = RequestData(id: 12, title: "Запит на вибірку з використанням агрегатної функції, умовою на агрегатну функцію, умовою на вибірку поля з сортуванням даних.")
//        let req13 = RequestData(id: 13, title: "Запит з використанням INNER JOIN.")
//        let req14 = RequestData(id: 14, title: "Запит з використанням LEFT JOIN.")
//        let req15 = RequestData(id: 15, title: "Запит з використанням RIGHT JOIN.")
//        let req16 = RequestData(id: 16, title: "Запит з використанням INNER JOIN і умовою")
//        let req17 = RequestData(id: 17, title: "Запит з використанням INNER JOIN і умовою LIKE")
//        let req18 = RequestData(id: 18, title: "Запит з використанням INNER JOIN і використанням агрегатної функції")
//        let req19 = RequestData(id: 19, title: "Запит з використанням INNER JOIN і використанням агрегатної функції і умови HAVING")
        let req20 = RequestData(id: 20, title: "Запит з використанням підзапита з використанням (=, <,>)")
        let req21 = RequestData(id: 21, title: "Запит з використанням підзапита з використанням агрегатної функції")
        let req23 = RequestData(id: 23, title: "Запит з використанням підзапита з використанням АNY або SOME")
        let req24 = RequestData(id: 24, title: "Запит з використанням підзапита з використанням IN")
        let req26 = RequestData(id: 26, title: "Запит на вибірку з використанням like")
        let req27 = RequestData(id: 27, title: "Запит на вибірку з двома умовами через 'and'")
        let req28 = RequestData(id: 28, title: "Запит на вибірку з двома умовами через «оr»")

        
        requests.append(req1)
        requests.append(req2)
        requests.append(req3)
        requests.append(req4)
        requests.append(req5)
        requests.append(req6)
        requests.append(req7)
        requests.append(req8)
        requests.append(req9)
        requests.append(req11)
        requests.append(req12)
//        requests.append(req13)
//        requests.append(req14)
//        requests.append(req15)
//        requests.append(req16)
//        requests.append(req17)
//        requests.append(req18)
//        requests.append(req19)
        requests.append(req20)
        requests.append(req21)
        requests.append(req23)
        requests.append(req24)
        requests.append(req26)
        requests.append(req27)
        requests.append(req28)


//        var premise1 = Premise()
//        premise1.id = incrementPremiseID()
//        premise1.owner = owner1
//        premise1.address = "City, street, flat1"
//        premise1.personalAccount = "1234567891"
//
//        owner1.premises.append(premise1)
//        try! self.realm.write({
//            self.realm.add(owner1)
//        })
        
//        var bill2 = Bill()
//        bill2.id = incrementBillID()
//        bill2.electricityBill = 22.9
//        bill2.gasBill = 140
//        bill2.heatingBill = 100
//        bill2.invoiceReceiptDate = Date()
//        bill2.isClosed = false
//        bill2.premise = premise1
//        try! self.realm.write({
//            self.realm.add(bill2)
//            self.realm.refresh()
//
//        })
        
//        var owner2 = Owner()
//        owner2.id = self.incrementOwnerID()
//        owner2.fullName = "Name Surname2"
//        owner2.passportId = "1234567892"
//        try! self.realm.write({
//            self.realm.add(owner2)
//        })
//        var owner3 = Owner()
//        owner3.id = self.incrementOwnerID()
//        owner3.fullName = "Name Surname3"
//        owner3.passportId = "1234567893"
//        try! self.realm.write({
//            self.realm.add(owner3)
//        })
//        var owner4 = Owner()
//        owner4.id = self.incrementOwnerID()
//        owner4.fullName = "Name Surname4"
//        owner4.passportId = "1234567894"
//        try! self.realm.write({
//            self.realm.add(owner4)
//        })
//        var owner5 = Owner()
//        owner5.id = self.incrementOwnerID()
//        owner5.fullName = "Name Surname5"
//        owner5.passportId = "1234567895"
//        try! self.realm.write({
//            self.realm.add(owner5)
//        })
//        var owner6 = Owner()
//        owner6.id = self.incrementOwnerID()
//        owner6.fullName = "Name Surname6"
//        owner6.passportId = "1234567896"
//        try! self.realm.write({
//            self.realm.add(owner6)
//        })

        

//        var premise2 = Premise()
//        premise2.id = incrementPremiseID()
//        premise2.owner = owner2
//        premise2.address = "City, street, flat2"
//        premise2.personalAccount = "1234567892"
//        try! self.realm.write({
//            self.realm.add(premise2)
//        })
//
//        var premise3 = Premise()
//        premise3.id = incrementPremiseID()
//        premise3.owner = owner3
//        premise3.address = "City, street, flat3"
//        premise3.personalAccount = "1234567893"
//        try! self.realm.write({
//            self.realm.add(premise3)
//        })
//
//        var premise4 = Premise()
//        premise4.id = incrementPremiseID()
//        premise4.owner = owner4
//        premise4.address = "City, street, flat4"
//        premise4.personalAccount = "1234567894"
//        try! self.realm.write({
//            self.realm.add(premise4)
//        })
//
//        var premise5 = Premise()
//
//        try! self.realm.write({
//            premise5.id = incrementPremiseID()
//            premise5.owner = owner4
//            premise5.address = "City, street, flat4"
//            premise5.personalAccount = "1234567894"
//            self.realm.add(premise5)
//        })
        
        
        
        
        
        
        
        
//        var bill3 = Bill()
//        bill3.id = incrementBillID()
//        bill3.electricityBill = 22.9
//        bill3.gasBill = 140
//        bill3.heatingBill = 100
//        bill3.invoiceReceiptDate = Date()
//        bill3.isClosed = false
//        bill3.premise = premise3
//        try! self.realm.write({
//            self.realm.add(bill3)
//        })
//
//        var bill4 = Bill()
//        bill4.id = incrementBillID()
//        bill4.electricityBill = 22.9
//        bill4.gasBill = 140
//        bill4.heatingBill = 100
//        bill4.invoiceReceiptDate = Date()
//        bill4.isClosed = false
//        bill4.premise = premise4
//        try! self.realm.write({
//            self.realm.add(bill4)
//        })
        
//        var bill5 = Bill()
//        bill5.id = incrementBillID()
//        bill5.electricityBill = 22.9
//        bill5.gasBill = 140
//        bill5.heatingBill = 100
//        bill5.invoiceReceiptDate = Date()
//        bill5.isClosed = false
//        bill4.premise = premise4
//        try! self.realm.write({
//            self.realm.add(bill5)
//        })
       





        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    func incrementOwnerID() -> Int {
        let realm = try! Realm()
        return (realm.objects(Owner.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    func incrementPremiseID() -> Int {
        let realm = try! Realm()
        return (realm.objects(Premise.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    func incrementBillID() -> Int {
        let realm = try! Realm()
        return (realm.objects(Bill.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return requests.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RequestTableViewCell
        cell.id = requests[indexPath.item].id
        cell.textLabel?.text = requests[indexPath.item].title
        
        return cell
    }
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! RequestTableViewCell
        self.selectedRequestID = cell.id
        performSegue(withIdentifier: "toData", sender: nil)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVC = segue.destination as! TableViewController
        destVC.requestID = self.selectedRequestID
    }
    

}
