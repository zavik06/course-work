//
//  Reminder.swift
//  Course work
//
//  Created by Влад  on 22.04.2020.
//  Copyright © 2020 Влад . All rights reserved.
//

import Foundation
import RealmSwift

class Reminder: Object {
    @objc dynamic var name = ""
    @objc dynamic var done = false
}
