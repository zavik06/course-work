//
//  OwnerCollectionViewCell.swift
//  Course work
//
//  Created by Влад  on 28.04.2020.
//  Copyright © 2020 Влад . All rights reserved.
//

import UIKit

class OwnerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var passportNumber: UILabel!
    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
