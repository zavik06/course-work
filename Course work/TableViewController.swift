//
//  TableViewController.swift
//  Course work
//
//  Created by Влад  on 22.04.2020.
//  Copyright © 2020 Влад . All rights reserved.
//

import UIKit
import RealmSwift

class TableViewController: UITableViewController {

    
    var requestID: Int?
    
    @IBOutlet weak var addBtn: UIBarButtonItem!
    var realm : Realm!
    
    var ownersList: Results<Owner> {
        get {
            return realm.objects(Owner.self)
        }
    }

    var premisesList: Results<Premise> {
        get {
            return realm.objects(Premise.self)
        }
    }

    var billsList: Results<Bill> {
        get {
            return realm.objects(Bill.self)
        }
    }
    
    var bills: Results<Bill>?
    var premises: Results<Premise>?
    var owners: Results<Owner>?
    var anyResult: [Any]?
    
    var anyBool: Bool?
    var avg: Double?
    var max: Double?
    var sum: Double?
    var min: Double?
    var count: Int?
    var maxBill: Bill?
    override func viewDidLoad() {
        super.viewDidLoad()
        realm = try! Realm()

        switch requestID {
            case 1:
                print("1")
            case 2:
                var premise = self.realm.objects(Premise.self).filter("id BETWEEN {0,2}")
                premises = premise
            case 3:
                var res = self.realm.objects(Premise.self).filter("id IN {1,4}")
                premises = res
                // REWORK FOR BILLS
            case 4:
                var res = self.realm.objects(Owner.self).distinct(by: ["fullName"])
                owners = res
            case 5:
                let maxValue = realm.objects(Bill.self).max(ofProperty: "gasBill") as Double?
                self.max = maxValue
            case 6:
                let sumValue = realm.objects(Bill.self).sum(ofProperty: "gasBill") as Double
                self.sum = sumValue
            case 7:
                let billsC = realm.objects(Premise.self).filter("bills.@count > 0")
                self.count = billsC.count
            case 8:
                let maxWaterBill = realm.objects(Bill.self).sorted(byKeyPath: "waterBill").last
                self.maxBill = maxWaterBill
            case 9:
                let billRes = realm.objects(Bill.self).filter("id BETWEEN {0,3}").value(forKeyPath: "gasBill") as! [Double]
                let avgGasBill = billRes.reduce(0, +) / Double(max(billRes.count, 1))
                self.avg = avgGasBill
            case 11:
                let billRes = realm.objects(Bill.self).filter("id BETWEEN {0,3} AND gasBill <= 300").min(ofProperty: "waterBill") as Double?
                self.min = billRes
            case 12:
                let billRes = realm.objects(Bill.self).filter("invoiceReceiptDate < \(Date()) AND electricityBill >= \((realm.objects(Bill.self).min(ofProperty: "electricityBill") as Double?)).unsafelyUnwrapped)").sorted(byKeyPath: "id")
                self.bills = billRes
            case 20:
                let gasBills = realm.objects(Bill.self).filter("gasBill > \((realm.objects(Bill.self).average(ofProperty: "gasBill") as Double?).unsafelyUnwrapped)")
                self.bills = gasBills
            case 21:
                let electricityBills = realm.objects(Bill.self).filter("electricityBill > \((realm.objects(Bill.self).min(ofProperty: "electricityBill") as Double?).unsafelyUnwrapped)")
                self.bills = electricityBills
            case 23:
                let heatingBills = realm.objects(Bill.self).filter("heatingBill > 400")
                if heatingBills.count > 0 {
                    anyBool = true
                } else {
                    anyBool = false
                }
            case 24:
                let ownersWithNames = realm.objects(Owner.self).filter("fullName IN {'Tim Cook', 'John Doe'}")
                self.owners = ownersWithNames
            case 26:
                var res = self.realm.objects(Owner.self).filter("passportId LIKE '*111?'")
                print(res)
                owners = res
            case 27:
                var res = self.realm.objects(Owner.self).filter("passportId LIKE '*4*' AND fullName CONTAINS 'Alex'")
                print(res)
                owners = res
            case 28:
                var res = self.realm.objects(Owner.self).filter("id == 2 or id == 3")
                print(res)
                owners = res
            default:
                print("не удалось распознать число")
        }
        realm = try! Realm()
        print(Realm.Configuration.defaultConfiguration.fileURL!)
    }
    
    func incrementOwnerID() -> Int {
        let realm = try! Realm()
        return (realm.objects(Owner.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    func incrementPremiseID() -> Int {
        let realm = try! Realm()
        return (realm.objects(Premise.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    func incrementBillID() -> Int {
        let realm = try! Realm()
        return (realm.objects(Bill.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch requestID {
            case 1:
                return ownersList.count
            case 2:
                return premises!.count
            case 3:
                return premises!.count
            case 4:
                return owners!.count
            case 5:
                return 1
            case 6:
                return 1
            case 7:
                return 1
            case 8:
                return 1
            case 9:
                return 1
            case 10:
                print("5")
            case 11:
                return 1
            case 12:
                return bills!.count
            case 20:
                return bills!.count
            case 21:
                return bills!.count
            case 23:
                return 1
            case 24:
                return owners!.count
            case 26:
                return owners!.count
            case 27:
                return owners!.count
            case 28:
                return owners!.count

            default:
                print("default")
        }
        return 10
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RequestTableViewCell
        switch requestID {
            case 1:
                cell.textLabel?.text = "Full name:" + ownersList[indexPath.item].fullName + " Passport number:" + ownersList[indexPath.item].passportId
                return cell
            
            case 2:
                let strPart = "Owner:" + premises![indexPath.item].owner!.fullName
                let strPart2 = " Address:" + premises![indexPath.item].address
                cell.textLabel?.text = strPart + strPart2 + " Personal account:" + premises![indexPath.item].personalAccount
                return cell
            case 3:
                let strPart = "Owner:" + premises![indexPath.item].owner!.fullName
                let strPart2 = " Address:" + premises![indexPath.item].address
                cell.textLabel?.text = strPart + strPart2 + " Personal account:" + premises![indexPath.item].personalAccount
                return cell
            case 4:
                cell.textLabel?.text = "Full name:" + owners![indexPath.item].fullName
                return cell
            case 5:
                cell.textLabel?.text = String(max!)
                return cell
            case 6:
                cell.textLabel?.text = String(sum!)
                return cell
            case 7:
                cell.textLabel?.text = String(count!)
                return cell
            case 8:
                
                let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

                let myString = formatter.string(from: maxBill!.invoiceReceiptDate) // string purpose I add here
                // convert your string to date
                let yourDate = formatter.date(from: myString)
                //then again set the date format whhich type of output you need
                formatter.dateFormat = "dd-MMM-yyyy"
                // again convert your date to string
                let myStringafd = formatter.string(from: yourDate!)

                cell.textLabel?.text = "ID:" + String(maxBill!.id) + " Water bill:" + String(maxBill!.waterBill) + " Gas bill:" + String(maxBill!.gasBill) + " Electricity bill:" + String(maxBill!.electricityBill) + " Heating bill:" + String(maxBill!.heatingBill) + " Date:" + String(myStringafd)
            case 9:
                cell.textLabel?.text = String(avg!)
                return cell
            case 11:
                cell.textLabel?.text = String(min!)
                return cell
            case 12:
                print("")
            case 20:
                let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

                let myString = formatter.string(from: bills![indexPath.item].invoiceReceiptDate) // string purpose I add here
                // convert your string to date
                let yourDate = formatter.date(from: myString)
                //then again set the date format whhich type of output you need
                formatter.dateFormat = "dd-MMM-yyyy"
                // again convert your date to string
                let myStringafd = formatter.string(from: yourDate!)

                cell.textLabel?.text = "ID:" + String(bills![indexPath.item].id) + " Water bill:" + String(bills![indexPath.item].waterBill) + " Gas bill:" + String(bills![indexPath.item].gasBill) + " Electricity bill:" + String(bills![indexPath.item].electricityBill) + " Heating bill:" + String(bills![indexPath.item].heatingBill) + " Date:" + String(myStringafd)
                return cell
            case 21:
                let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

                let myString = formatter.string(from: bills![indexPath.item].invoiceReceiptDate) // string purpose I add here
                // convert your string to date
                let yourDate = formatter.date(from: myString)
                //then again set the date format whhich type of output you need
                formatter.dateFormat = "dd-MMM-yyyy"
                // again convert your date to string
                let myStringafd = formatter.string(from: yourDate!)

                cell.textLabel?.text = "ID:" + String(bills![indexPath.item].id) + " Water bill:" + String(bills![indexPath.item].waterBill) + " Gas bill:" + String(bills![indexPath.item].gasBill) + " Electricity bill:" + String(bills![indexPath.item].electricityBill) + " Heating bill:" + String(bills![indexPath.item].heatingBill) + " Date:" + String(myStringafd)
                return cell
            case 23:
                cell.textLabel?.text = String(anyBool!)
            case 24:
                cell.textLabel?.text = "Full name:" + owners![indexPath.item].fullName + " Passport number:" + owners![indexPath.item].passportId
                return cell
            case 26:
                cell.textLabel?.text = "Full name:" + owners![indexPath.item].fullName + " Passport number:" + owners![indexPath.item].passportId
                return cell
            case 27:
                cell.textLabel?.text = "Full name:" + owners![indexPath.item].fullName + " Passport number:" + owners![indexPath.item].passportId
                return cell
            case 28:
                 cell.textLabel?.text = "Full name:" + owners![indexPath.item].fullName + " Passport number:" + owners![indexPath.item].passportId
                 return cell
            default:
                print("не удалось распознать число")
        }

        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView,
                      didSelectRowAt indexPath: IndexPath) {
    }
    

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        }
    
    }
    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


