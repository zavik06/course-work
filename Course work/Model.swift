//
//  Model.swift
//  Course work
//
//  Created by Влад  on 26.04.2020.
//  Copyright © 2020 Влад . All rights reserved.
//
//
import Foundation
import RealmSwift

class Owner: Object{
    @objc dynamic var id = 0
    @objc dynamic var fullName = ""
    @objc dynamic var passportId = ""
    let premises = List<Premise>()
    override static func primaryKey() -> String? {
        return "id"
    }
}

class Premise: Object{
    @objc dynamic var id = 0
    @objc dynamic var owner: Owner? = nil
    @objc dynamic var address = ""
    @objc dynamic var personalAccount = ""
    let bills = List<Bill>()

}

class Bill: Object{
    @objc dynamic var id = 0
    @objc dynamic var premise: Premise? = nil
    @objc dynamic var invoiceReceiptDate = Date()
    @objc dynamic var gasBill = 0.0
    @objc dynamic var waterBill = 0.0
    @objc dynamic var electricityBill = 0.0
    @objc dynamic var heatingBill = 0.0
    @objc dynamic var isClosed = false
}
